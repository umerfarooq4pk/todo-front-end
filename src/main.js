import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import axios from 'axios';

Vue.config.productionTip = false
const BASE_URL = process.env.VUE_APP_BASE_URL;

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate, { fieldsBagName: 'veeFields' });

import myMixins from '@/mixins/notify.js';
Vue.mixin(myMixins)


// if(localStorage.getItem('todo_token') != null ){
//   axios.get(`${BASE_URL}/allpermissionstorole`).then(res => {
//     app_instance();
//   }).catch(err => {
//       console.log(err);
//       app_instance();
//   });
// }else{
//   app_instance();
// }
app_instance();
function app_instance(){
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}
