export default {
    methods: {
        showError(err){
            let errors = err.response.data.errors;
            let errArr = [];
            Object.keys(errors).map((field) => {
                errArr.push(errors[field][0]);
            });
            errArr.forEach(element => {
                this.$notify({
                    title: 'Error',
                    message: element,
                    type: 'error',
                    position: 'bottom-right',
                });
            })
        },
        notify(message){
            this.$notify({
                title: 'Success',
                message: message,
                type: 'success',
                position: 'bottom-right',
            });
        }
    }
}