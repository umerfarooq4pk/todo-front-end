import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Welcome from '../views/Welcome.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Todo from '../views/Todo.vue'
Vue.use(VueRouter)
let routes = [];
if(localStorage.getItem('todo_token')){
  routes = [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/add-todo',
      name: 'New Todo',
      component: Todo
    },
    {
      path: '/edit-todo/:id',
      name: 'Edit Todo',
      component: Todo
    },
  ]
}else{
  routes = [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
  ]
}

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  // redirect unauthenticated user to login page if trying to access a restricted page
  const publicPages = ['/login', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('todo_token');

  if (loggedIn && publicPages.includes(to.path)) {
    router.push('/');
  }else{
    next();
  }
})

export default router
