import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
Vue.use(Vuex)

const BASE_URL = process.env.VUE_APP_BASE_URL;
let user = JSON.parse(localStorage.getItem('todo_user'));
const instance = axios.create({
  headers: {
    'Authorization': 'Bearer '+localStorage.getItem('todo_token'),
    'user_id': (user) ? user.id : 0,
  }
});
export default new Vuex.Store({
  state: {
    todo: [],
  },
  mutations: {
    SET_TODO(state, payload){
      state.todo = payload;
    },
    DELETE_TODO(state, id){
      let index = state.todo.findIndex(item => item.id == id);
      state.todo.splice(index, 1);
    },
  },
  getters: {
    getTodos(state){
      return state.todo;
    }
  },
  actions: {
    async login({commit}, payload){
      let res = await axios.post(`${BASE_URL}/login`, payload);
      localStorage.setItem('todo_token', res.data.access_token);
      localStorage.setItem('todo_user', JSON.stringify(res.data.user));
      window.location.reload();
      return res;
    },
    async register({commit}, payload){
      let res = await axios.post(`${BASE_URL}/signup`, payload);
      return res;
    },
    async getTodos({commit}){
      let res = await instance.get(`${BASE_URL}/todo`);
      console.log('res data', res.data);
      commit('SET_TODO', res.data);
      return res;
    },
    async addTodo({commit}, payload){
      let res = await instance.post(`${BASE_URL}/todo`, payload);
      console.log('todo res', res.data);
      return res;
    },
    async getTodo({commit}, id){
      let res = await instance.get(`${BASE_URL}/todo/${id}`);
      return res;
    },
    async updateTodo({commit}, payload){
      let res = await instance.put(`${BASE_URL}/todo/${payload.id}`, payload);
      return res;
    },
    async deleteTodo({commit}, id){
      let res = await instance.delete(`${BASE_URL}/todo/${id}`);
      commit('DELETE_TODO', id);
      return res;
    }
  },
  modules: {
  }
})
